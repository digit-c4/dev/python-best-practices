# Python best practices

* Use [`Pyenv`](https://github.com/pyenv/pyenv) for multiple python version management
* preferred Python version: 3.11
* Use [`venv`](https://docs.python.org/3/library/venv.html) for virtual environment management
* Use [`Poetry`](https://python-poetry.org/) for package management
* Use [src layout](https://packaging.python.org/en/latest/discussions/src-layout-vs-flat-layout/) (module root dir in `/src`) to avoid side effects during editable installation.
* put test scripts in `/tests` directory. Group it between `unit/`, `integration/`, etc.

You may also be interested in
* [Generic best practices](https://code.europa.eu/digit-c4/dev/best-practices)

## Setup workstation
The Workstation setup can be executed thanks to the [configure_vm role](https://code.europa.eu/digit-c4/dev/ansible-collection/-/tree/main/roles/configure_vm?ref_type=heads) of the dev ansible-collection. The manual steps are
1. install `Pyenv` globally
   ```shell
   curl https://pyenv.run | bash
   ```
   and configure to use python 3.11
   ```shell
   pyenv install 3.11
   pyenv global 3.11
   ```
3. Install `Poetry` globally.
    ```shell
    curl -sSL https://install.python-poetry.org | python3 -
    ```

## Bootstrap
```shell
mkdir my-project && cd my-project
# Make sure the standard .gitignore is set
curl -o .gitignore https://raw.githubusercontent.com/github/gitignore/main/Python.gitignore
# init and activate virtual environment
python3 -m venv ./venv && source venv/bin/activate
# init Poetry. It will rely only the previously created venv, if activated.
poetry init -q
```

## Containerization
The python-best-practices Poetry image should be used
```Dockerfile
FROM code.europa.eu:4567/digit-c4/dev/python-best-practices/python-poetry:3.10-alpine

RUN poetry install
```

Available tags:
* `3.8-alpine`
* `3.10-alpine`
* `3.11-alpine`

## Testing

### Static tests
We recommend three libraries for static testing
* [flake8](https://flake8.pycqa.org/en/latest/), as a tool for style check and code quality. Some project may use Pylint
  but it is not advised as it has the reputation to generate a lot of false positive.
* [black](https://github.com/psf/black) for formatting
* [isort](https://pycqa.github.io/isort/) for formatting the imports

```shell
# Add dependencies to test group
poetry add --group test flake8 flake8-annotation isort black
# Make test group optional (only run when the group is first created)
echo "[tool.poetry.group.test]\noptional = true" >> pyproject.toml
# install test dependency group
poetry install --only test
# Using Flake8 for lint
poetry run flake8 src
# Sort the imports
poetry run isort --multi-line 3 --profile black ./src
# Enforce styling
poetry run black ./src
# Using pytype for type checking
poetry run #TODO
```

> Sources:
> * https://sefidian.com/2021/08/03/how-to-use-black-flake8-and-isort-to-format-python-codes/
> * https://inventwithpython.com/blog/2022/11/19/python-linter-comparison-2022-pylint-vs-pyflakes-vs-flake8-vs-autopep8-vs-bandit-vs-prospector-vs-pylama-vs-pyroma-vs-black-vs-mypy-vs-radon-vs-mccabe/

VSCode remote development will sync when the following extensions are installed on the remote server
* Flake8 `v2023.10.0`
* Pylance `v2023.12.1`
* Python `v2023.22.1`
* Python Debugger `v2024.2.0`

### Testing framework
```shell
# Add dependencies to test group
poetry add --group test pytest
# Make test group optional (only run when the group is first created)
echo "[tool.poetry.group.test]\noptional = true" >> pyproject.toml
# install test dependency group
poetry install --only test
# Run pytest
poetry run pytest
```

## Pipelines
This project provides three plug and play Gitlab CI jobs
* build and publish Python package on Gitlab Python registry
* check the Python style using flake8
* enforces Python style using autopep8 and push

### Build and publish python package
A docker based CI job for building and publishing Python packages 
```yaml
stages:
  - build

include:
  - project: 'digit-c4/dev/python-best-practices'
    file: 'gitlab-ci/build-and-publish-package.yml'
    ref: v3.0

build-and-publish:
  extends: .build-and-publish-package-job-template
  stage: build
  tags:
    - docker
  variables:
    # path to the project root (default is git root)
    # PROJECT_PATH: "./"
  
    # perform a dry run "yes" (default) or "no". Mind the quotes as YAML parses yes 
    # as boolean, and Gitlab only expects String for variables.
    # IMPORTANT: must be specified for actual publishing as default value is "no"
    # DRY_RUN: "yes"
```

### **Ensures** Python style
A job to scan Python files in a given directory (default `/src`) and succeed only if it matches the Python style definition.
It is using [flake8](https://flake8.pycqa.org/en/latest/) under the hood and can be configured using the 
standard `.flake8` file
```yaml
stages:
  - test

include:
  - project: 'digit-c4/dev/python-best-practices'
    file: 'gitlab-ci/test-static-style-check.yml'
    ref: v3.0

style-check:
  extends: .test-static-style-check-job-template
  stage: test
  tags:
    - docker
  variables:
    #SRC_PATH: ./src
```

### **Enforces** Python style
A more aggressive version of the previous job because it will update the code to enforces styling (when possible),
commit, and push the changes. Developers are therefore incentivised to check styling before pushing if they want
to avoid any pull/push conflict.

For the job to work, a Gitlab token must be provided. The default `CI_TOKEN` will indeed not have the right to push changes.
1. browse the `/settings/access_tokens` control panel of the project
2. Create a repository token with write access and developer role
3. Copy the value in a `CI_TOKEN_WRITE` CI variable
   
   ![ci-variable.png](doc%2Fci-variable.png)

```yaml
stages:
  - test

include:
  - project: 'digit-c4/dev/python-best-practices'
    file: 'gitlab-ci/test-static-style-force.yml'
    ref: v3.0

force-style:
  stage: test
  extends: .test-static-style-force-job-template
  tags:
    - docker
  variables:
    CI_JOB_TOKEN_WRITE: ${CI_TOKEN_WRITE}
    #SRC_PATH: ./src
```

## Inspiring trainings, documents, videos, etc
* [🎥 Python Tutorial: VENV (Mac &amp; Linux) - How to Use Virtual Environments with the Built-In venv Module](https://www.youtube.com/watch?v=Kg1Yvry_Ydk)
* [🎥 Python Tutorial: Pipenv - Easily Manage Packages and Virtual Environments](https://www.youtube.com/watch?v=zDYL22QNiWk)
* [🎥 5 Common Python Mistakes and How to Fix Them](https://www.youtube.com/watch?v=zdJEYhA2AZQ)
* [🎥 10 Python Tips and Tricks For Writing Better Code](https://www.youtube.com/watch?v=C-gEQdGVXbk)
* [🎥 Python Tutorial: Unit Testing Your Code with the unittest Module](https://www.youtube.com/watch?v=6tNS--WetLI)
