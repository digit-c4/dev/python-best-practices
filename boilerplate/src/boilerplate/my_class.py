"""
A documentation for the module
"""


class MyClass:
    """
    A documentation for the class
    """

    def __init__(self):
        """
        A documentation for the method
        """

    def do_something(self):
        """
        print this class name
        """
        print(self.__class__.__name__)

    def do_something_else(self):
        """
        print something else
        """
        print(self.__class__.__base__)
