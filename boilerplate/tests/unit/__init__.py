import logging
import unittest

try:
    import my_package
except ModuleNotFoundError:
    logging.debug("package was not installed and will be loaded locally")

    import sys
    import os

    sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../../src")

    import my_package


from .test_my_class import TestStringMethods

if __name__ == '__main__':
    unittest.main()
