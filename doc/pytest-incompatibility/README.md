# Pytest not compatible with `py` package name

When using Pytest, no package `py` can be created, or the following error will be displayed as soon as the 
package is to be imported

> `ImportError: cannot import name 'v1' from 'py' (/Users/raphaeljoie/Workspace/code.europa.eu/digit-c4/dev/python-best-practices/doc/pytest-incompatibility/demo/venv/lib/python3.11/site-packages/py.py)`

## How to Reproduce

```shell
pyenv install 3.8
pyenv local 3.8
python3 -m venv venv
source venv/bin/activate
poetry install
python
> # working
> from lib import py
> # not working
> from py import v1
```
Then
```shell
poetry remove pytest
python
> # working
> from lib import py
> # working!!!!
> from py import v1
```
