ARG PYTHON_TAG=3.11

FROM python:${PYTHON_TAG} as build

RUN wget https://install.python-poetry.org -O install.py
RUN python3 install.py

FROM python:${PYTHON_TAG}

COPY --from=build /root/.local/ /root/.local/

# add Poetry install path
ENV PATH="/root/.local/bin:$PATH"

# Poetry will use Dulwich library to load git dependencies without git CLI
# The library unfortunately doesn't work with secured proxy. We therefore
# need to fall back on the native git CLI. This option of Poetry is enabled
# using the POETRY_EXPERIMENTAL_SYSTEM_GIT_CLIENT environment variable
ENV POETRY_EXPERIMENTAL_SYSTEM_GIT_CLIENT=true

RUN poetry config virtualenvs.create false
