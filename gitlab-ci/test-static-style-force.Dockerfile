FROM code.europa.eu:4567/digit-c4/dev/python-best-practices/python-poetry:3.11-alpine

RUN apk add git
RUN pip install autopep8
RUN git config --global user.email "bot@code.europa.eu" \
    && git config --global user.name "code.europa.eu Bot" \
